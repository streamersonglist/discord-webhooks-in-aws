import { WebhookClient, RichEmbed } from 'discord.js';
const DEDENT = require('dedent');

const getEnv = (variable: string): string => {
  if (process.env.hasOwnProperty(variable)) {
    return process.env[variable] as string;
  }
  throw new Error(`Environment variable ${variable} is not set`);
};

const GITLAB_URL = 'https://gitlab.com';

// Colors corresponding to different events
const ColorCodes = {
  issue_opened: 15426592, // orange
  issue_closed: 5198940, // grey
  issue_comment: 15109472, // pale orange
  commit: 7506394, // blue
  release: 2530048, // green
  merge_request_opened: 12856621, // red
  merge_request_closed: 2530048, // green
  merge_request_comment: 15749300, // pink
  default: 5198940, // grey
  error: 16773120, // yellow
  red: 12856621,
  green: 2530048,
  grey: 5198940,
};

const StrLen = {
  title: 128,
  description: 128,
  field_name: 128,
  field_value: 128,
  commit_id: 8,
  commit_msg: 32,
  json: 256,
  snippet_code: 256,
};

const dateOptions = {
  //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleString
  hour12: true,
  weekday: 'short',
  day: 'numeric',
  month: 'numeric',
  year: 'numeric',
  hour: 'numeric',
  minute: 'numeric',
  timeZoneName: 'short',
};

export class GitlabEvents {
  private webhookClient = new WebhookClient(getEnv('DISCORD_ID'), getEnv('DISCORD_TOKEN'));

  public async processData(type: string, data: any) {
    console.log('processing...');

    let embed = new RichEmbed({
      color: ColorCodes.default,
      timestamp: new Date(),
      footer: {
        text: 'GitLab Webhook',
      },
    });

    // Set up common values, if they exist
    if (data.user) {
      embed.setAuthor(
        this.truncate(data.user.username) || this.truncate(data.user.name),
        this.getAvatarUrl(data.user.avatar_url),
      );
    } else {
      embed.setAuthor(
        this.truncate(data.user_username) || this.truncate(data.user_name),
        this.getAvatarUrl(data.user_avatar),
      );
    }
    if (data.project) {
      embed.setURL(this.truncate(data.project.web_url));
      embed.setTitle(`[${data.project.path_with_namespace}] ${type}`);
    }

    try {
      switch (type) {
        case 'Push Hook':
          embed = this.pushHook(embed, data);
          break;

        case 'Tag Push Hook':
          embed = this.pushTagsHook(embed, data);
          break;

        case 'Issue Hook':
        case 'Confidential Issue Hook':
          embed = this.issueHook(embed, data);
          break;

        case 'Note Hook':
          embed = this.noteHook(embed, data);
          break;

        case 'Merge Request Hook':
          embed = this.mergeRequestHook(embed, data);
          break;

        case 'Wiki Page Hook':
          embed = this.wikiPageHook(embed, data);
          break;

        case 'Pipeline Hook':
          embed = this.pipelineHook(embed, data);
          break;

        case 'Build Hook':
        case 'Job Hook':
          embed = this.buildOrJobHook(embed, data, type);
          break;

        case 'Fake Error':
          console.log('# Invoked a Fake Error response.');
          embed.description = data.fake.error;
          break;

        case 'Known Error':
          embed.setColor(ColorCodes.error);
          embed.setTitle('Error Processing HTTP Request');
          embed.description = data.message;

          if (data.body) {
            embed.addField('Received Data', this.truncate(data.body, StrLen.field_value));
          }

          break;

        default:
          // TODO
          console.log('# Unhandled case! ', type);
          embed.setTitle(`Type: ${type}`);
          embed.description = `This feature is not yet implemented`;

          embed.addField('Received Data', this.truncate(JSON.stringify(data), StrLen.json));

          break;
      }
    } catch (e) {
      console.log('Error Context: processing data of an HTTP request. Type: ' + type);
      console.error(e);

      embed.setColor(ColorCodes.error);
      embed.setTitle('Error Reading HTTP Request Data: ' + type);
      embed.description = e.message;
    }

    // Send data via webhook
    await this.sendData(embed);
  }

  private pushHook(embed: RichEmbed, data: any): RichEmbed {
    embed.setColor(ColorCodes.commit);

    if (data.commits.length < 1) {
      console.log(JSON.stringify(data));
    } else if (data.commits.length == 1) {
      embed.setDescription(DEDENT`
        **1 New Commit**\n
        ${data.commits[0].message}\n
        ${data.commits[0].modified.length} change(s)\n
        ${data.commits[0].added.length} addition(s)\n
        ${data.commits[0].removed.length} deletion(s)
      `);
    } else {
      let desc = `**${data.total_commits_count} New Commits**\n`;
      for (let i = 0; i < Math.min(data.commits.length, 5); i++) {
        let changelog = DEDENT`
        ${data.commits[i].modified.length} change(s)
        ${data.commits[i].added.length} addition(s)
        ${data.commits[i].removed.length} deletion(s)
        `;
        desc += `[${this.truncate(data.commits[i].id, StrLen.commit_id, true)}](${
          data.commits[i].url
        } '${changelog}') ${this.truncate(data.commits[i].message, StrLen.commit_msg)} - ${
          data.commits[i].author.name
        }\n`;
      }
      embed.setDescription(desc);
    }

    return embed;
  }

  private pushTagsHook(embed: RichEmbed, data: any): RichEmbed {
    embed.setDescription(`**Tag ${data.ref.substring('refs/tags/'.length)}**\n`);
    embed.setURL(`${data.project.web_url}/${data.ref}`);

    // Commit Stuff
    if (data.commits.length < 1) {
      console.log(JSON.stringify(data));
    } else if (data.commits.length == 1) {
      embed.description += DEDENT`
      ${data.commits[0].message}\n
      ${data.commits[0].modified.length} change(s)\n
      ${data.commits[0].added.length} addition(s)\n
      ${data.commits[0].removed.length} deletion(s)
      `;
    } else {
      for (let i = 0; i < Math.min(data.commits.length, 5); i++) {
        let changelog = DEDENT`
        ${data.commits[i].modified.length} change(s)
        ${data.commits[i].added.length} addition(s)
        ${data.commits[i].removed.length} deletion(s)
        `;
        embed.description += `[${this.truncate(data.commits[i].id, StrLen.commit_id, true)}](${
          data.commits[i].url
        } '${changelog}') ${this.truncate(data.commits[i].message, StrLen.commit_msg)} - ${
          data.commits[i].author.name
        }\n`;
      }
    }
    // Tag Stuff
    embed.addField(
      'Previous',
      `[${this.truncate(data.before, StrLen.commit_id, true)}](${data.project.web_url}/commit/${
        data.before
      } 'Check the previous tagged commit')`,
      true,
    );
    embed.addField(
      'Current',
      `[${this.truncate(data.after, StrLen.commit_id, true)}](${data.project.web_url}/commit/${
        data.after
      } 'Check the current tagged commit')`,
      true,
    );

    return embed;
  }

  private issueHook(embed: RichEmbed, data: any): RichEmbed {
    embed.setURL(this.truncate(data.object_attributes.url));
    let action = 'Issue';

    switch (data.object_attributes.action) {
      case 'open':
        embed.setColor(ColorCodes.issue_opened);
        action = 'Issue Opened:';
        break;
      case 'close':
        embed.setColor(ColorCodes.issue_closed);
        action = 'Issue Closed:';
        break;
      default:
        embed.setColor(ColorCodes.issue_comment);
        console.log('## Unhandled case for Issue Hook ', data.object_attributes.action);
        break;
    }

    if (data.object_attributes.confidential) {
      // TODO support multiple hooks for private and public updates
      embed.description += `**${action} [CONFIDENTIAL]**\n`;
    } else {
      embed.description += `**${action} #${data.object_attributes.iid} ${
        data.object_attributes.title
      }**\n`;
      embed.description += this.truncate(data.object_attributes.description, StrLen.description);

      if (data.assignees && data.assignees.length > 0) {
        let assignees = { inline: true, name: 'Assigned To:', value: '' };
        for (let i = 0; i < data.assignees.length; i++) {
          assignees.value += `${data.assignees[i].username}\n`;
        }
        embed.addField(assignees.name, assignees.value, assignees.inline);
      }

      if (data.labels && data.labels.length > 0) {
        let labels = { inline: true, name: 'Labeled As:', value: '' };
        for (let i = 0; i < data.labels.length; i++) {
          labels.value += `${data.labels[i].title}\n`;
        }
        embed.addField(labels.name, labels.value, labels.inline);
      }
    }
    return embed;
  }

  private noteHook(embed: RichEmbed, data: any): RichEmbed {
    embed.setURL(data.object_attributes.url);

    embed.addField('Comment', this.truncate(data.object_attributes.note, StrLen.field_value));

    switch (data.object_attributes.noteable_type) {
      case 'commit':
      case 'Commit':
        embed.setColor(ColorCodes.commit);
        embed.description = `**New Comment on Commit ${this.truncate(
          data.commit.id,
          StrLen.commit_id,
          true,
        )}**\n`;

        let commit_info = `[${this.truncate(data.commit.id, StrLen.commit_id, true)}](${
          data.commit.url
        }) `;
        commit_info += `${this.truncate(data.commit.message, StrLen.commit_msg, false, true)} - ${
          data.commit.author.name
        }`;
        embed.addField('Commit', commit_info);

        let commit_date = new Date(data.commit.timestamp);
        embed.addField(
          'Commit Timestamp',
          // Given Format: 2014-02-27T10:06:20+02:00
          commit_date.toLocaleString('UTC', dateOptions),
        );
        break;

      case 'merge_request':
      case 'MergeRequest':
        embed.setColor(ColorCodes.merge_request_comment);

        let mr_state = data.merge_request.state ? `[${data.merge_request.state}]` : '';
        embed.description = DEDENT`
                **New Comment on Merge Request #${data.merge_request.iid}**
                *Merge Status: ${data.merge_request.merge_status}* ${mr_state}
                ${data.merge_request.title}`;

        let last_commit_info = `[${this.truncate(
          data.merge_request.last_commit.id,
          StrLen.commit_id,
          true,
        )}](${data.merge_request.last_commit.url}) `;
        last_commit_info += `${this.truncate(
          data.merge_request.last_commit.message,
          StrLen.commit_msg,
          false,
          true,
        )} - ${data.merge_request.last_commit.author.name}`;
        embed.addField('Latest Commit', last_commit_info);

        embed.addField('Assigned To', this.truncate(data.merge_request.assignee.username));

        let mr_date = new Date(data.merge_request.created_at);
        embed.addField(
          'Merge Request Timestamp',
          // Given Format: 2014-02-27T10:06:20+02:00
          mr_date.toLocaleString('UTC', dateOptions),
        );

        break;

      case 'issue':
      case 'Issue':
        embed.setColor(ColorCodes.issue_comment);

        let issue_state = data.issue.state ? ` [${data.issue.state}]` : '';
        embed.description = `**New Comment on Issue #${data.issue.iid} ${
          data.issue.title
        } ${issue_state}**\n`;

        let issue_date = new Date(data.issue.created_at);
        embed.addField(
          'Issue Timestamp',
          // Given Format: 2014-02-27T10:06:20+02:00
          issue_date.toLocaleString('UTC', dateOptions),
        );

        break;

      case 'snippet':
      case 'Snippet':
        embed.description = `**New Comment on Code Snippet**\n`;

        embed.addField('Title', this.truncate(data.snippet.title, StrLen.field_value), true);

        embed.addField(
          'File Name',
          this.truncate(data.snippet.file_name, StrLen.field_value),
          true,
        );

        let snip_filetype = data.snippet.file_name.substr(
          data.snippet.file_name.lastIndexOf('.') + 1,
        );
        embed.addField(
          'Code Snippet',
          '```' +
            snip_filetype +
            '\n' +
            this.truncate(data.snippet.content, StrLen.snippet_code) +
            '\n```',
        );

        let snip_date = new Date(data.snippet.created_at);
        embed.addField(
          'Snippet Timestamp',
          // Given Format: 2014-02-27T10:06:20+02:00
          snip_date.toLocaleString('UTC', dateOptions),
        );
        break;

      default:
        console.log('## Unhandled case for Note Hook ', data.object_attributes.noteable_type);
        break;
    }
    return embed;
  }

  private mergeRequestHook(embed: RichEmbed, data: any): RichEmbed {
    embed.setURL(data.object_attributes.url);
    embed.setTitle(`[${data.object_attributes.target.path_with_namespace}] Merge Request Hook`);

    switch (data.object_attributes.action) {
      case 'open':
        embed.setColor(ColorCodes.merge_request_opened);
        embed.description = `**Merge Request Opened: #${data.object_attributes.iid} ${
          data.object_attributes.title
        }**\n`;
        break;
      case 'close':
        embed.setColor(ColorCodes.merge_request_closed);
        embed.description = `**Merge Request Closed: #${data.object_attributes.iid} ${
          data.object_attributes.title
        }**\n`;
        break;
      default:
        embed.setColor(ColorCodes.merge_request_comment);
        console.log('## Unhandled case for Merge Request Hook ', data.object_attributes.action);
        break;
    }

    embed.description += DEDENT`
            *Merge Status: ${data.object_attributes.merge_status}* [${data.object_attributes.state}]
            ${this.truncate(data.object_attributes.description, StrLen.description)}
            `;

    embed.addField(
      'Merge From',
      DEDENT`
              ${data.object_attributes.source.namespace}/
              ${data.object_attributes.source.name}:
              [${data.object_attributes.source_branch}](${data.object_attributes.source.web_url})`,
      true,
    );

    embed.addField(
      'Merge Into',
      DEDENT`
              ${data.object_attributes.target.namespace}/
              ${data.object_attributes.target.namespace}:
              [${data.object_attributes.target_branch}](${data.object_attributes.target.web_url})`,
      true,
    );

    /*if (data.object_attributes.source) {
            embed.addField({
              name: 'Source:',
              value: `[${data.object_attributes.source.path_with_namespace}: ${data.object_attributes.source_branch}](${data.object_attributes.source.web_url} '${data.object_attributes.source.name}')`
            });
          } 
          if (data.object_attributes.target) {
            embed.addField({
              name: 'Target:',
              value: `[${data.object_attributes.target.path_with_namespace}: ${data.object_attributes.target_branch}](${data.object_attributes.target.web_url} '${data.object_attributes.target.name}')`
            });
          }*/

    if (data.object_attributes.assignee) {
      embed.addField('Assigned To', `${data.object_attributes.assignee.username}`, true);
    }

    if (data.assignees && data.assignees.length > 0) {
      let assignees = { inline: true, name: 'Assigned To:', value: '' };
      for (let i = 0; i < data.assignees.length; i++) {
        assignees.value += `${data.assignees[i].username}\n`;
      }
      embed.addField(assignees.name, assignees.value, assignees.inline);
    }

    if (data.labels && data.labels.length > 0) {
      let labels = { inline: true, name: 'Labeled As:', value: '' };
      for (let i = 0; i < data.labels.length; i++) {
        labels.value += `${data.labels[i].title}\n`;
      }
      embed.addField(labels.name, labels.value, labels.inline);
    }
    return embed;
  }

  private wikiPageHook(embed: RichEmbed, data: any): RichEmbed {
    embed.setURL(data.object_attributes.url);
    embed.description = `**Wiki Action: ${data.object_attributes.action}**\n`;
    embed.description += this.truncate(data.object_attributes.message, StrLen.description);

    embed.addField('Page Title', data.object_attributes.title);

    if (data.object_attributes.content) {
      embed.addField('Page Content', this.truncate(data.object_attributes.content, 128));
    }
    return embed;
  }

  private pipelineHook(embed: RichEmbed, data: any): RichEmbed {
    embed.description = `**Pipeline Status Change** [${data.object_attributes.status}]\n`;

    let status_emote = '';

    switch (data.object_attributes.status) {
      case 'failed':
        embed.setColor(ColorCodes.red);
        status_emote = '❌ ';
        break;
      case 'created':
      case 'success':
        embed.setColor(ColorCodes.green);
        status_emote = '✅ ';
        break;
      default:
        embed.setColor(ColorCodes.grey);
        break;
    }

    embed.addField('Duration', this.msToTime(data.object_attributes.duration * 1000));

    let commit_info = `[${this.truncate(data.commit.id, StrLen.commit_id, true)}](${
      data.commit.url
    }) `;
    commit_info += `${this.truncate(data.commit.message, StrLen.commit_msg, false, true)} - ${
      data.commit.author.name
    }`;
    embed.addField('Commit', commit_info);

    if (data.builds && data.builds.length > 0) {
      for (let i = 0; i < data.builds.length; i++) {
        let dates = {
          create: new Date(data.builds[i].created_at),
          start: new Date(data.builds[i].started_at),
          finish: new Date(data.builds[i].finished_at),
        };
        let emote = '';
        if (data.builds[i].status == 'failed') emote = '❌';
        if (data.builds[i].status == 'skipped') emote = '↪️';
        if (data.builds[i].status == 'success' || data.builds[i].status == 'created') emote = '✅';

        let build_link = `[${data.builds[i].id}](${data.project.web_url +
          '/-/jobs/' +
          data.builds[i].id})`;

        let build_details = `*Skipped Build ID ${build_link}*`;

        if (data.builds[i].status != 'skipped') {
          build_details = DEDENT`
                - **Build ID**: ${build_link}
                - **User**: [${data.builds[i].user.username}](${GITLAB_URL}/${
            data.builds[i].user.username
          })
                - **Created**: ${dates.create.toLocaleString('UTC', dateOptions)}
                - **Started**: ${dates.start.toLocaleString('UTC', dateOptions)}
                - **Finished**: ${dates.finish.toLocaleString('UTC', dateOptions)}`;
        }
        embed.addField(
          `${emote} ${this.truncate(data.builds[i].stage)}: ${this.truncate(data.builds[i].name)}`,
          build_details,
        );
      }
    }
    return embed;
  }

  private buildOrJobHook(embed: RichEmbed, data: any, type: string): RichEmbed {
    // For some reason GitLab doesn't send user data to job hooks, so set username/avatar to empty
    embed.setAuthor('', '');
    // It also doesn't include the project web_url ??? or the path with namespace ???
    let canon_url = data.repository.git_http_url.slice(0, -'.git'.length);
    let namespace = canon_url.substr(GITLAB_URL.length + 1);

    embed.setTitle(`[${namespace}] ${type}`);
    embed.description = `**Job: ${data.build_name}**\n`;
    embed.setURL(`${canon_url}/-/jobs/${data.build_id}`);

    embed.addField('Duration', this.msToTime(data.build_duration * 1000));

    let build_commit_info = `[${this.truncate(
      data.commit.sha,
      StrLen.commit_id,
      true,
    )}](${canon_url}/commit/${data.commit.sha}) `;
    build_commit_info += `${this.truncate(data.commit.message, StrLen.commit_msg, false, true)} - ${
      data.commit.author_name
    }`;
    embed.addField('Commit', build_commit_info);

    let build_dates = {
      start: new Date(data.build_started_at),
      finish: new Date(data.build_finished_at),
    };

    let build_emote = '';
    switch (data.build_status) {
      case 'failed':
        embed.setColor(ColorCodes.red);
        build_emote = '❌';
        break;
      case 'created':
      case 'success':
        embed.setColor(ColorCodes.green);
        build_emote = '✅';
        break;
      case 'skipped':
        embed.setColor(ColorCodes.grey);
        build_emote = '↪️';
        break;
      default:
        embed.setColor(ColorCodes.grey);
        break;
    }

    let build_link = `[${data.build_id}](${embed.url})`;
    let build_details = `*Skipped Build ID ${build_link}*`;
    if (data.build_status != 'skipped') {
      build_details = DEDENT`
      - **Build ID**: ${build_link}
      - **Commit Author**: [${data.commit.author_name}](${data.commit.author_url})
      - **Started**: ${build_dates.start.toLocaleString('UTC', dateOptions)}
      - **Finished**: ${build_dates.finish.toLocaleString('UTC', dateOptions)}`;
    }
    embed.addField(
      `${build_emote} ${this.truncate(data.build_stage)}: ${this.truncate(data.build_name)}`,
      build_details,
    );

    return embed;
  }

  private async sendData(embed: RichEmbed) {
    console.log('sending...');

    await this.webhookClient
      .send('', { embeds: [embed] })
      .then((message) => console.log(`Sent embed`))
      .catch((error) =>
        console.log(`[sendData] Sending an embed via WebHook: ${this.webhookClient.name}`),
      );
  }

  private msToTime(s: number) {
    // Pad to 2 or 3 digits, default is 2
    var pad = (n, z = 2) => ('00' + n).slice(-z);
    return (
      pad((s / 3.6e6) | 0) +
      'h:' +
      pad(((s % 3.6e6) / 6e4) | 0) +
      'm:' +
      pad(((s % 6e4) / 1000) | 0) +
      '.' +
      pad(s % 1000, 3) +
      's'
    );
  }

  private getAvatarUrl(str: string) {
    if (str == null) return '';
    if (str.startsWith('/')) return GITLAB_URL + str;
    return str;
  }

  private truncate(str: string, count = 0, noElipses = false, noNewLines = false) {
    if (noNewLines) str = str.split('\n').join(' ');
    if (!count && str) return str;
    if (count && str && noElipses) {
      return str.substring(0, count);
    } else if (str && str.length > 0) {
      if (str.length <= count) return str;
      return str.substring(0, count - 3) + '...';
    } else {
      return '';
    }
  }
}
