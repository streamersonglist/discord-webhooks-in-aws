# Discord Webhooks in AWS

This project deploys an API Gateway route and a Lambda function to handle webhook events from GitLab and post them to a Discord webhook.

This is made easy by the open source [Serverless](https://serverless.com/) framework.

Much of the processing of the GitLab webhook events was provided by the following project: [Discord-Gitlab-Webhook](https://github.com/FlyingKatsu-Discord-Bots/Discord-GitLab-Webhook)

## Setup

### The following environment variables need to be set in GitLab's CI / CD settings:

- AWS_ACCESS_KEY_ID
- AWS_DEFAULT_REGION
- AWS_SECRET_ACCESS_KEY
- DISCORD_ID
- DISCORD_TOKEN
