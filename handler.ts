import { GitlabEvents } from './gitlab-events';
import { APIGatewayProxyEvent, Context } from 'aws-lambda';

const handleGitlab = async (
  event: APIGatewayProxyEvent,
  context: Context,
  callback: (error?: any, response?: any) => void,
) => {
  const gitlabEvents = new GitlabEvents();
  try {
    if (event.body && event.headers.hasOwnProperty('X-Gitlab-Event')) {
      await gitlabEvents.processData(event.headers['X-Gitlab-Event'], JSON.parse(event.body));
    } else {
      console.log(event);
    }
  } catch (error) {
    console.log(error);
  }

  context.succeed({
    statusCode: '200',
  });

  callback(null);
};

export { handleGitlab };
